#ifndef GUITEST_H
#define GUITEST_H

#include <QtWidgets/QMainWindow>
#include <QObject>
#include "ui_guitest.h"

#include "DataCollector.h"
#include "qcustomplot.h"


// Global collector
extern DataCollector collector;


class GUItest : public QMainWindow
{
	Q_OBJECT

public:
	GUItest(QWidget *parent = 0);
	~GUItest();

	QCustomPlot *customPlot;
	

private:
	Ui::GUItestClass ui;
	int timerId;

protected:
	void timerEvent(QTimerEvent *event);
	void updateLCDs();
	void updateGraph();
};


#endif
