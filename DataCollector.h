#ifndef DATAC_H
#define DATAC_H

#include <stdlib.h>

#include <array>
#include <vector>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include <windows.h>
#include <conio.h>

#include <myo/myo.hpp>

#define AVE_WINDOW 200

using namespace myo;

class DataCollector : public myo::DeviceListener
{

public:
	Hub hub;
	Myo* myo;

	// Constructor
	DataCollector()
		: emgSamples(), emgSum(0), emgMax(0), emgMin(100), emgAveSum(0), currentPose(), printData(false),
		hub("me.davidfarrow.myo"), movingArray(), movingArrayIndex(0), ave(0)
	{
		//hub("me.davidfarrow.myo");	
	}

	void WaitKey();

	// onUnpair() is called whenever the Myo is disconnected from Myo Connect by the user.
	void onUnpair(myo::Myo* myo, uint64_t timestamp);

	// onEmgData() is called whenever a paired Myo has provided new EMG data, and EMG streaming is enabled.
	void onEmgData(myo::Myo* myo, uint64_t timestamp, const int8_t* emg);

	// onPose() is called whenever the Myo detects that the person wearing it has changed their pose, for example,
	// making a fist, or not making a fist anymore.
	void onPose(myo::Myo* myo, uint64_t timestamp, myo::Pose pose);

	// We define this function to print the current values that were updated by the on...() functions above.
	void print();


	// The values of this array is set by onEmgData() above.
	std::array<int8_t, 8> emgSamples;

	// Set by onPose()
	myo::Pose currentPose;

	// Set when " " gesture is activated/deactivated 
	bool printData;

	// Sum of EMG data
	int emgSum;

	// Highest measurement of the sum of EMG data
	int emgMax;

	// Lowest measurement of the sum of EMG data
	int emgMin;

	// Average measurement of the sum of EMG data
	long movingArrayIndex;

	std::array<int, AVE_WINDOW> movingArray;

	int ave;

	int emgAveSum;

};

#endif