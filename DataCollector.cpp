#include "DataCollector.h"

// Global collector
DataCollector collector;


void DataCollector::WaitKey()
{
	std::cout << "\t\t\t     Press ENTER to continue...";
	while (_kbhit()) _getch(); // Empty the input buffer
	_getch(); // Wait for a key
	while (_kbhit()) _getch(); // Empty the input buffer (some keys sends two messages)
}


void DataCollector::onUnpair(myo::Myo* myo, uint64_t timestamp)
{
	emgSamples.fill(0);
	emgAveSum = 0;
	emgMax = 0;
	emgMin = 0;
	emgSum = 0;

	ave = 0;
	movingArrayIndex = 0;
	movingArray.fill(0);
}


void DataCollector::onEmgData(myo::Myo* myo, uint64_t timestamp, const int8_t* emg)
{
	for (int i = 0; i < 8; i++)
	{
		emgSamples[i] = emg[i];
	}
}


void DataCollector::onPose(myo::Myo* myo, uint64_t timestamp, myo::Pose pose)
{
	currentPose = pose;

	if (pose != myo::Pose::unknown && pose != myo::Pose::rest) {
		// Tell the Myo to stay unlocked until told otherwise. We do that here so you can hold the poses without the
		// Myo becoming locked.
		myo->unlock(myo::Myo::unlockHold);

		// Notify the Myo that the pose has resulted in an action, in this case changing
		// the text on the screen. The Myo will vibrate.
		myo->notifyUserAction();
	}
	else {
		// Tell the Myo to stay unlocked only for a short period. This allows the Myo to stay unlocked while poses
		// are being performed, but lock after inactivity.
		myo->unlock(myo::Myo::unlockTimed);
	}

	// If a fist is detected we update the printData boolean
	if (currentPose == myo::Pose::fist)
	{
		if (printData)
		{
			printData = false;
		}
		else
		{
			printData = true;
			Sleep(500);
		}
	}

	// If fingers spread we reset the data 
	if (currentPose == myo::Pose::fingersSpread)
	{
		emgSamples.fill(0);
		emgAveSum = 0;
		emgMax = 0;
		emgMin = 0;
		emgSum = 0;
		
		ave = 0;
		movingArrayIndex = 0;
		movingArray.fill(0);
	}
}


void DataCollector::print()
{
	emgSum = 0;

	int magnitude;
	int maximum = 0;

	// Print out the EMG data.
	for (size_t i = 0; i < emgSamples.size(); i++)
	{
		magnitude = static_cast<int>(emgSamples[i]);

		// Make all the values positive
		if (magnitude < 0)
		{
			magnitude = ((-1)*magnitude);
		}

		emgSum += magnitude;

		int index = movingArrayIndex % AVE_WINDOW;

		emgAveSum -= movingArray[index];

		movingArray[index] = emgSum;

		emgAveSum += movingArray[index];

		ave = emgAveSum / AVE_WINDOW;

		movingArrayIndex++;


		// Compare Max measurement
		if (emgSum > emgMax)
		{
			emgMax = emgSum;
		}

		// Compare Minimum Measurement 
		if (emgSum != 0 && emgSum < emgMin)
		{
			emgMin = emgSum;
		}

	}

	Beep((ave * 30), 25);
}