#include "guitest.h"
#include <QtWidgets/QApplication>
#include "DataCollector.h"

//#include "qcustomplot.h"
//#include "chart.h"
//#include <QtCharts/QChartView>

//QT_CHARTS_USE_NAMESPACE

using namespace myo;

// Global collector
extern DataCollector collector;


int main(int argc, char *argv[])
{
	try
	{
		std::cout << "Attempting to find a Myo..." << std::endl;

		// Attempt to find a Myo to use. If a Myo is already paired in Myo Connect, this will return that Myo immediately.
		// waitForMyo() takes a timeout value in milliseconds. If it fails, the function will return a null pointer.
		collector.myo = collector.hub.waitForMyo(10000);

		// If waitForMyo() returned a null pointer, we failed to find a Myo, so exit with an error message.
		if (!collector.myo)
		{
			throw std::runtime_error("Unable to find a Myo!");
		}

		// We've found a Myo.
		std::cout << "Connected to a Myo armband!" << std::endl << std::endl;

		// Next we enable EMG streaming on the found Myo.
		collector.myo->setStreamEmg(myo::Myo::streamEmgEnabled);

		// Register the DeviceListener for Hub::run() to send events
		collector.hub.addListener(&collector);

	}
	catch (const std::exception& e)
	{
		std::cerr << "Error: " << e.what() << std::endl;
		std::cerr << "Press enter to continue.";
		std::cin.ignore();
	}



	// Start GUI
	QApplication a(argc, argv);
	GUItest w;

	/*Chart *chart = new Chart;
	chart->setTitle("Dynamic spline chart");
	chart->legend()->hide();
	chart->setAnimationOptions(QChart::AllAnimations);
	QChartView chartView(chart);
	chartView.setRenderHint(QPainter::Antialiasing);
	w.setCentralWidget(&chartView);*/
	

	//// generate some data:
	//QVector<double> x(101), y(101); // initialize with entries 0..100
	//for (int i = 0; i<101; ++i)
	//{
	//	x[i] = i / 50.0 - 1; // x goes from -1 to 1
	//	y[i] = x[i] * x[i]; // let's plot a quadratic function
	//}
	//// create graph and assign data to it:
	////w.customPlot->graph();
	//w.customPlot->addGraph();
	//w.customPlot->graph(0)->setData(x, y);
	//// give the axes some labels:
	//w.customPlot->xAxis->setLabel("x");
	//w.customPlot->yAxis->setLabel("y");
	//// set axes ranges, so we see all data:
	//w.customPlot->xAxis->setRange(-1, 1);
	//w.customPlot->yAxis->setRange(0, 1);
	//w.customPlot->replot();




	//w.customPlot->addGraph(); // blue line
	//w.customPlot->graph(0)->setPen(QPen(Qt::blue));
	//w.customPlot->graph(0)->setBrush(QBrush(QColor(240, 255, 200)));
	//w.customPlot->graph(0)->setAntialiasedFill(false);
	//w.customPlot->addGraph(); // red line
	//w.customPlot->graph(1)->setPen(QPen(Qt::red));
	//w.customPlot->graph(0)->setChannelFillGraph(w.customPlot->graph(1));

	//w.customPlot->addGraph(); // blue dot
	//w.customPlot->graph(2)->setPen(QPen(Qt::blue));
	//w.customPlot->graph(2)->setLineStyle(QCPGraph::lsNone);
	//w.customPlot->graph(2)->setScatterStyle(QCPScatterStyle::ssDisc);
	//w.customPlot->addGraph(); // red dot
	//w.customPlot->graph(3)->setPen(QPen(Qt::red));
	//w.customPlot->graph(3)->setLineStyle(QCPGraph::lsNone);
	//w.customPlot->graph(3)->setScatterStyle(QCPScatterStyle::ssDisc);

	//w.customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
	//w.customPlot->xAxis->setDateTimeFormat("hh:mm:ss");
	//w.customPlot->xAxis->setAutoTickStep(false);
	//w.customPlot->xAxis->setTickStep(2);
	//w.customPlot->axisRect()->setupFullAxesBox();

	//w.customPlot->xAxis;

	// make left and bottom axes transfer their ranges to right and top axes:
	//connect(w.customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
	//connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));

	// setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
	//connect(&dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
	//dataTimer.start(0); // Interval 0 means to refresh as fast as possible




	/*Chart *chart = new Chart;
	chart->setTitle("Dynamic spline chart");
	chart->legend()->hide();
	chart->setAnimationOptions(QChart::AllAnimations);
	QChartView chartView(chart);
	chartView.setRenderHint(QPainter::Antialiasing);
	w.setCentralWidget(&chartView);*/


	w.show();
	return a.exec();
}
