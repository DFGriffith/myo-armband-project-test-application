#include "guitest.h"
//#include "main.cpp"

#include <array>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#include <windows.h>
#include <conio.h>

#include <myo/myo.hpp>


#include <QDebug>





GUItest::GUItest(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	//-- GUi update timer for timer event
	timerId = startTimer(50);


	//-- Setup graph
	ui.graph->addGraph(); // line
	ui.graph->graph(0)->setPen(QPen(Qt::blue));

	ui.graph->addGraph(); // dot
	ui.graph->graph(1)->setPen(QPen(Qt::blue));
	ui.graph->graph(1)->setLineStyle(QCPGraph::lsNone);
	ui.graph->graph(1)->setScatterStyle(QCPScatterStyle::ssDisc);

	ui.graph->xAxis->setTickLabelType(QCPAxis::ltDateTime);
	ui.graph->xAxis->setDateTimeFormat("hh:mm:ss");
	ui.graph->xAxis->setAutoTickStep(false);
	ui.graph->xAxis->setTickStep(2);
	ui.graph->axisRect()->setupFullAxesBox();

	// make left and bottom axes transfer their ranges to right and top axes:
	connect(ui.graph->xAxis, SIGNAL(rangeChanged(QCPRange)), ui.graph->xAxis2, SLOT(setRange(QCPRange)));
	connect(ui.graph->yAxis, SIGNAL(rangeChanged(QCPRange)), ui.graph->yAxis2, SLOT(setRange(QCPRange)));
}


GUItest::~GUItest()
{
	killTimer(timerId);
}


void GUItest::timerEvent(QTimerEvent *event)
{

	// In each iteration of our main loop, we run the Myo event loop for a set number of milliseconds.
	// In this case, we wish to update our display 50 times a second, so we run for 1000/20 milliseconds.
	collector.hub.run(1000 / 20);
	// After processing events, we call the print() member function we defined above to print out the values we've
	// obtained from any events that have occurred.
	if (collector.printData)
	{
		collector.print();

		updateLCDs();

		updateGraph();
	}
			
	qApp->processEvents();
}


void GUItest::updateGraph()
{
	// calculate two new data points:
	double key = QDateTime::currentDateTime().toMSecsSinceEpoch() / 1000.0;
	static double lastPointKey = 0;
	if (key - lastPointKey > 0.01) // at most add point every 10 ms
	{
		// add data to lines:
		ui.graph->graph(0)->addData(key, collector.ave);
		// set data of dots:
		ui.graph->graph(1)->clearData();
		ui.graph->graph(1)->addData(key, collector.ave);

		// remove data of lines that's outside visible range:
		ui.graph->graph(0)->removeDataBefore(key - 8);
		// rescale value (vertical) axis to fit the current data:
		ui.graph->graph(0)->rescaleValueAxis();
		lastPointKey = key;
	}
	// make key axis range scroll with the data (at a constant range size of 8):
	ui.graph->xAxis->setRange(key + 0.25, 8, Qt::AlignRight);
	ui.graph->replot();
}


void GUItest::updateLCDs()
{

	// LCD Sum
	ui.lcdSum->display(collector.emgSum);

	// LCD Max
	ui.lcdMax->display(collector.emgMax);
	
	// LCD Min
	ui.lcdMin->display(collector.emgMin);

	// LCD Ave
	ui.lcdAve->display(collector.ave);
	

	// LCD/EMG 1
	if (collector.emgSamples[0] > 0)
		ui.lcdEMG_1->display(collector.emgSamples[0]);
	else
		ui.lcdEMG_1->display(0);

	// LCD/EMG 2
	if (collector.emgSamples[1] > 0) 
		ui.lcdEMG_2->display(collector.emgSamples[1]);
	else
		ui.lcdEMG_2->display(0);

	// LCD/EMG 3
	if (collector.emgSamples[2] > 0)
		ui.lcdEMG_3->display(collector.emgSamples[2]);
	else
		ui.lcdEMG_3->display(0);

	// LCD/EMG 4
	if (collector.emgSamples[3] > 0)
		ui.lcdEMG_4->display(collector.emgSamples[3]);
	else
		ui.lcdEMG_4->display(0);

	// LCD/EMG 5
	if (collector.emgSamples[4] > 0)
		ui.lcdEMG_5->display(collector.emgSamples[4]);
	else
		ui.lcdEMG_5->display(0);

	// LCD/EMG 6
	if (collector.emgSamples[5] > 0)
		ui.lcdEMG_6->display(collector.emgSamples[5]);
	else
		ui.lcdEMG_6->display(0);

	// LCD/EMG 7
	if (collector.emgSamples[6] > 0)
		ui.lcdEMG_7->display(collector.emgSamples[6]);
	else
		ui.lcdEMG_7->display(0);

	// LCD/EMG 8
	if (collector.emgSamples[7] > 0)
		ui.lcdEMG_8->display(collector.emgSamples[7]);
	else
		ui.lcdEMG_8->display(0);
}